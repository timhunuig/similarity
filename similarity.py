import time
import multiprocessing
from multiprocessing import Pool
import numpy as np
import re
import math
from functools import partial
import torch
from Bio import SeqIO
import os

device = 'cuda' if torch.cuda.is_available() else 'cpu'

def prefix_span(smin, lmin, patterns, pot_maximal, block, charset=['C', 'T', 'G', 'A']):
    # Modified prefix pan algorithm to find pattern occurence within a sequence
  
    extended_patterns = list()

    for pattern in patterns:
        for char in charset:
            beta = pattern + char

            # find where each pattern starts in the string
            indices = [m.start() for m in re.finditer(beta, block)]
            # number of occurences for a pattern beta
            no_occurences = len(indices)
            
            # if the number of occurences are greater than or equal to the minimum support (smin):
            # it is potentially maximal if it is greater than the minimum length (lmin)
            if no_occurences >= smin:
                if len(beta) >= lmin:
                    pot_maximal.append([beta, no_occurences])
                    extended_patterns.append(beta)
                else:
                    extended_patterns.append(beta)
            else:
                continue

    if len(patterns) != 0:
        # each recursion explores patterns of length +1 
        return prefix_span(smin, lmin, extended_patterns, pot_maximal, block)
    else:
        return pot_maximal
    
def vector_getter(seq, B=200, smin=2, lmin=3):    
    # Function using the methodology from https://bmcgenomics.biomedcentral.com/articles/10.1186/1471-2164-16-S3-S5#Sec2

    # seq - DNA sequence (str);
    # B - length of block;
    # smin - the minimum support threshold;
    # lmin - the minimum pattern length;

    N = len(seq)//B
    
    # initialising vector that holds entropy scores for each block
    vector = np.array([])
    
    # sequence blocking - segment the DNA sequence in B length blocks
    blocks = list()
    for i in range(N):
        block = seq[i*B:(i+1)*B]
        blocks.append(block)
    
    # get max number of processors to divide block processing process
    processors = multiprocessing.cpu_count() - 1 
    pool = Pool(processes=processors)
    
    # initialising list to hold list of potential maximal patterns for each block
    pot_maximals = list()
    
    for i in range(N//processors):
        block_interval = blocks[i*processors:(i+1)*processors]
        func = partial(prefix_span, smin, lmin, ['C', 'T', 'G', 'A'], list())
        result = pool.map(func, block_interval)
        for lst in result:
            pot_maximals.append(lst)
    
    # perform prefix span for remaining blocks
    for block in blocks[(N//processors)*processors:len(blocks)]:
        lst = prefix_span(smin, lmin, ['C', 'T', 'G', 'A'], list(), block)
        pot_maximals.append(lst)

    for pot_maximal in pot_maximals:
        maximals = list()
        # find maximals in each potentially maximal list by seeing if pattern is a subsequence of another pattern
        for maximal in pot_maximal:
            without = [x for x in pot_maximal if x != maximal]
            isMaximal = True
            for item in without:
                if maximal[0] in item[0]:
                    isMaximal = False
                    break
            if isMaximal:
                maximals.append(maximal)
        
        # entropy calculation
        sum_of_spat = 0 # sum of support of pattern 
        product_sum = 0 # sum of all (pattern support * pattern probability * ln(pattern probability))
        for maximal in maximals:
            # maximal[0] = pattern, maximal[1] = number of occurences of pattern (support of pattern)
            sum_of_spat += maximal[1]
            p = maximal[1]/(B-len(maximal[0]) + 1) # probability of pattern
            product_sum += maximal[1]*p*math.log(p)
        
        # avoid division by zero error
        if sum_of_spat == 0:
            H = 0
        else:
            H = -1*(1/sum_of_spat)*product_sum # entropy
            
        # the entropies of all blocks constitutes dimensional components of vector for the sequence
        vector = np.append(vector, H)
    
    return vector

if __name__ == '__main__':
    
    S = list() # store similarities 
    E = list() # store latent representation distances

    for _ in range(100):
        random_choice_1 = random.choice(df['strain'])
        random_choice_2 = random.choice(df['strain'])

        while random_choice_1 == random_choice_2:
            random_choice_2 = random.choice(df['strain'])

        if os.path.isfile(f'AGORA2_Genomes/{random_choice_1}.fna') and os.path.isfile(f'AGORA2_Genomes/{random_choice_2}.fna'):

            seq1 = ''
            for record in SeqIO.parse(f'AGORA2_Genomes/{random_choice_1}.fna', "fasta"):
                seq1 += str(record.seq)

            seq2 = ''
            for record in SeqIO.parse(f'AGORA2_Genomes/{random_choice_2}.fna', "fasta"):
                seq2 += str(record.seq)

            vec1 = vector_getter(seq1)
            vec2 = vector_getter(seq2)
            
            rand_vec1 = vec1
            rand_vec2 = vec2

            avg_sim = []
            for __ in range(100):
                
                # ensure vectors are same length but record initial lengths
                if len(rand_vec1) > len(rand_vec2):
                    n = len(rand_vec1)
                    m = len(rand_vec2)
                    rand_vec1 = rand_vec1[:len(rand_vec2)]
                else:
                    n = len(rand_vec2)
                    m = len(rand_vec1)
                    rand_vec2 = rand_vec2[:len(rand_vec1)]
                
                # perform similarity calculation on gpu if available
                rand_vec1 = torch.tensor(rand_vec1).to(device)
                rand_vec2 = torch.tensor(rand_vec2).to(device)

                similarity = (n/m)*torch.sqrt(sum((rand_vec1 - rand_vec2)**2))
                similarity = float(similarity.cpu().numpy())
                
                rand_vec1 = vec1
                rand_vec2 = vec2
                
                # randomly shuffle vector to compare multiple different arrangements
                # ?? debatable component ??
                random.shuffle(rand_vec1)
                random.shuffle(rand_vec2)

                avg_sim.append(similarity)
            
            # similarity = averaged distance sequences are from each other
            similarity = sum(avg_sim)/len(avg_sim)
            dist = distance(df, random_choice_1, random_choice_2)

            S.append(similarity)
            E.append(dist)

            print(f"({_}) Similarity and Distance between of {random_choice_1} & {random_choice_2} is: {similarity} & {dist}\n")